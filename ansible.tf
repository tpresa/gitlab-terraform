resource "local_file" "AnsibleInventory" {
    content = templatefile("inventory.tmpl", {
      public-dns = aws_instance.gitlab.public_dns,
      public-ip = aws_instance.gitlab.public_ip,
      private-id = aws_instance.gitlab.id
    }
  )
  filename = "hosts"
}

resource "local_file" "AnsibleVariables" {
    content = templatefile("vars.yml.tmpl", {
      public-dns = aws_instance.gitlab.public_dns,
      public-ip = aws_instance.gitlab.public_ip,
      private-id = aws_instance.gitlab.id
    }
  )
  filename = "vars.yml"
}
