provider "aws" {
  version = "~> 2.0"
  region     = var.region
}

terraform {
  backend "http" {}
}
