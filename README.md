# How to deploy GitLab with this project

This project uses Terraform for cloud provisioning, and Ansible for configuration management. 

The CI pipeline of this project deploys GitLab. It contains the following jobs:

* init
* validate
* plan
* apply
* ansible
* destroy

The steps above essentially call their terraform counterpart, with the exception of `ansible`. `This job uses ansible to configure the instance with GitLab.

The jobs `apply`, `ansible`, and `destroy` are marked as `when:manual`, to prevent unintended modifications.

Steps to perform the deployment would be:

1. Run the pipeline
2. Once the automated steps are finished, run `apply`
3. Once `apply` is finished, run `ansible`

If something goes south, or you'd like to tear down what was provisioned, you only need to run `destroy`.

The AWS account being used is set via CI variables. If you wish to deploy to a different account, you'll need to change `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` under Settings > CI/CD > Variables