resource "aws_security_group" "gitlab" {
  name = "tpresa-gitlab-terraform-sg"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "deployer_key" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCXOXvRFObZNw7Z5eAtL4+ZtgC33UhSpmtVVNejwVJSKEFRacTySHj1MfaUvzOCyvS8RelifMiuZOsLBfFHWOb4J/6nIdoeU/du/NX3lO7E2VvnDV3KWnTxIvsJEzUihUc0WIX0cefEoBuoqyRL3Ze1jU7eqtNPE/q3SGGP2/qQGVf96eftW3eNcIuCtNup91pjfOOqYrtPXrWJeVTSnpbGJ4bOK+xLgwffhpI5wW8RZFOQf8MniarQ1WL12flCgDzGj5+kxbSYAs0Edx5qpBhocYbOrOamJMcJZkGJnjtW4Ej8x0eoTtIqFWF0C5nKgNOZXK3DQDMl9cahGoZ30/xBE+YWOD/vrhgyxGzPRvyM4RaYuQ9r89VAawJeaWx0UuHAB/QovUYcz/uwbvRczT4Y/tPrt7WSpF6GWfRcEfl6Pr01aIQ+gZpj5y1Z9dRPt3GanjAZroF9HX1aXY0s903f6bM3Nf88/+VpVx9Irp9ncoao7voDdLgtVgMxEL1zWpugt1+wSdF2oHM4snIcaAi/ORqWFI9I2UeLI8K2LKKvC7vFyHLhxroALapjLiUuo5ZgDI7YQxYU8ST6UjZp4AJFGrN8YMiMLzhRD2v4XX71ZKeuzX0yIpj8FDh/7Ie/VFfendNh+s18lRPoF6LRdFtDzqg+TDRBbf7sVLrTfPnwCw=="
}

resource "aws_instance" "gitlab" {
  ami           = "ami-085cf0dc25cd48a89"
  instance_type = "c5.xlarge"
  vpc_security_group_ids = [aws_security_group.gitlab.id]
  key_name = aws_key_pair.deployer_key.key_name

  tags = {
    Name = "tpresa-gitlab-terraform"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get -qq install python -y"
    ]
    connection {
      host        = self.public_ip
      type        = "ssh"
      user        = "ubuntu"
    }
  }
}
